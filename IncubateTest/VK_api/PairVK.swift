//
//  PairVK.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 25.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import Foundation

struct Pair {
    let key: String
    var value: String
}
