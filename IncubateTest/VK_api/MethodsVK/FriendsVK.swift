//
//  FriendsVK.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 25.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import Foundation
class FriendsVK {

    var countOfSearch = 0
    let surl = "https://api.vk.com/method/friends.get"
    let userDefaults = UserDefaults.standard
    let version = "v=5.69"
    var response: ResponseOfSearch?
    var parameter: [Pair] = [Pair(key: "user_ids", value: ""),
                             Pair(key: "order", value: "random"),
                             Pair(key: "count", value: "20"),
                             Pair(key: "offset", value:"0"),
                             Pair(key:"fields", value:"sex,screen_name,photo_200_orig"),
                             Pair(key: "name_case", value: "nom"),
                             Pair(key:"access_token", value: UserDefaults.standard.string(forKey: "VKAccessToken")!)]
    
    func getCountFriends()-> Int {
        return countOfSearch
    }
    
    func getFriendsList(userID: String, offset: Int, count: Int, completion: @escaping ([User]) -> Void) {
        guard let url = URL(string: surl) else { return }
        parameter[0].value = userID
        parameter[2].value = String(count)
        parameter[3].value = String(offset)
       
        //data request
        var strURL = ""
        for p in parameter {
            strURL += "\(p.key)=\(p.value)&"
        }
        strURL += version
        print(strURL)
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        
        guard let httpBody = strURL.data(using: .utf8) else { return }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            guard let data = data else {return}
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
                self.response = try JSONDecoder().decode(ResponseOfSearch.self, from: data)
                
                self.countOfSearch = self.response!.response.count
                for i in 0..<self.response!.response.items.count {
                    
                    self.response!.response.items[i].photo = try Data(contentsOf: URL(string: (self.response?.response.items[i].photo_200_orig)!)!)
                    
                    
                }
                completion(self.response!.response.items)
                
            } catch let error {
                
                print("\(error)")
                
            }
            }.resume()
        
        
    }
}
