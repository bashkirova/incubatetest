//
//  AuthorizationController.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 22.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import Foundation

class AuthorizationController {
    
    static func authIsSuccess(request: URLRequest) -> Bool {
        
        let requestString = request.url?.absoluteString
        var temp = requestString
        if requestString?.range(of: "access_token") != nil {
            var key = "access_token="
            let accessToken = self.stringBetweenString(startString: key, endString: "&", innerString: (requestString)!)
            key = "user_id="
            //get user id
            temp!.removeFirst(temp!.distance(from: temp!.startIndex, to: temp!.index(of: key)!))
            temp!.removeFirst(key.count)
            let user_id = temp
           
            //save keys
            let userDefaults = UserDefaults.standard
            userDefaults.set(user_id, forKey: "VKAccessUserId")
            userDefaults.set(accessToken,forKey: "VKAccessToken")
            userDefaults.set(true, forKey: "VKIsAccess")
            userDefaults.synchronize()
            
            return true
        } else {
            return false
        }
    }
    
    
    //method cutString between Strings
    
    private static func stringBetweenString(startString: String, endString: String, innerString: String) -> String?{
        
        guard let indexOfStartString = innerString.index(of: startString) else {return nil}
        var tempString = innerString
        tempString.removeFirst(innerString.distance(from: innerString.startIndex, to: indexOfStartString))
       
        tempString.removeFirst(startString.count)
  
        guard let indexOfEndString = tempString.index(of: endString)  else { return nil}
       
        tempString.removeLast(tempString.distance(from: indexOfEndString, to: tempString.endIndex))
    
        return tempString
    }
   
}
