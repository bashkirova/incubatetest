//
//  VKWebView.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 22.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import UIKit

class VKWebView: UIViewController , UIWebViewDelegate {
    
    var viewController: StartViewController!
    var webView: UIWebView! = nil
    
    var appID = "6265688"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if webView == nil {
            self.webView = UIWebView(frame: self.view.bounds)
            self.webView.delegate = self
            self.webView.scalesPageToFit = true
            self.view = webView
        }
        
        let stringURL = "https://oauth.vk.com/authorize?client_id="
            + self.appID
            + "&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends,offline&response_type=token&v=5.52"
        let url = URL(string: stringURL)
        webView.loadRequest(URLRequest(url: url!))
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        webView.stopLoading()
        webView.delegate = nil
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.url
        
        if url?.absoluteString == "https://oauth.vk.com/blank.html#error=access_denied&error_reason=user_denied&error_description=User%20denied%20your%20request" {
            super.dismiss(animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if AuthorizationController.authIsSuccess(request: webView.request!) {
            viewController!.authComplete()
            self.dismiss(animated: true, completion: nil)
            
        } else if ((webView.request?.url?.absoluteString.range(of: "error")) != nil) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
