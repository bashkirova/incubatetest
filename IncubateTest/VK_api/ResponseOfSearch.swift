//
//  ResponseOfSearch.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 22.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import Foundation

struct ResponseOfGet: Decodable {
    var response: [User]
}
struct ResponseOfSearch:Decodable {
    var response: Response
}
struct User:Decodable {
    var first_name: String
    var id: Int
    var last_name: String
    var photo_200_orig: String
    var screen_name: String
    var sex: Int
    var photo: Data?
}
struct Response:Decodable {
    var count: Int
    var items: [User]
}
