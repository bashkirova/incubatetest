//
//  ProfileTableViewCell.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 22.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
