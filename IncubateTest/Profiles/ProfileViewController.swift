//
//  ProfileViewController.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 22.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    
    var person: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if person != nil {
            avatarImageView.image = UIImage(data: person!.photo!)
            avatarImageView.clipsToBounds = true
        }
        setupNavigationBar()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ProfileTableViewCell
        
        switch indexPath.row {
        case 0:
            
            cell.keyLabel.text = "Имя"
            cell.valueLabel.text = person!.first_name
        case 1:
            
            cell.keyLabel.text = "Фамилия"
            cell.valueLabel.text = person!.last_name
        case 2:
            cell.keyLabel.text = "Screenname"
            cell.valueLabel.text = person!.screen_name
        case 3:
            cell.keyLabel.text = "Пол"
            cell.valueLabel.text = person!.sex == 2 ? "Мужик" : "Прекрасная леди"
        default:
            break
        }
        return cell
    }
    
    func setupNavigationBar() {
        navigationItem.largeTitleDisplayMode = .never
    }

}
