//
//  SearchFriendsTableTableViewController.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 25.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import UIKit

class SearchFriendsTableTableViewController: UITableViewController {
    
    
    private var friendsVK       = FriendsVK()
    private var friends: [User] = []

    private var currentPage     = 0
    private var shouldShowLoadingCell = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl                  = UIRefreshControl()
        refreshControl?.backgroundColor = #colorLiteral(red: 0.3163684011, green: 0.6845170259, blue: 0.8394302726, alpha: 1)
        refreshControl?.tintColor       = .white
        refreshControl?.addTarget(self, action: #selector(reloadSceneData), for: .valueChanged)
        
        definesPresentationContext      = true
        refreshControl?.beginRefreshing()
        
        loadResults()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard isLoadingIndexPath(indexPath) else { return }
        fetchNextPage()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = friends.count
        return shouldShowLoadingCell ? count + 1 : count
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == self.friends.count
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func personsToDisplay(indexPath: IndexPath) -> User {
        return friends[indexPath.row]
    }
    @objc func reloadSceneData() {
        currentPage = 0
        loadResults(refresh: true)
    }
    private func fetchNextPage() {
        currentPage += 1
        loadResults()
    }
    private func loadResults(refresh: Bool = false) {
        let userDef = UserDefaults.standard
        
        guard let userID = userDef.string(forKey: "VKAccessUserId") else {return}
        friendsVK.getFriendsList(userID: userID , offset: 20*currentPage, count: 20) { (users) in
            DispatchQueue.main.async {
                if refresh {
                    self.friends = users
                } else {
                    self.friends.append(contentsOf: users)
                }
                self.shouldShowLoadingCell = self.currentPage < self.friendsVK.getCountFriends()/20 + 1
                
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if isLoadingIndexPath(indexPath) {
            
            return LoadingCell(style: .default, reuseIdentifier: "loading")
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PersonTableViewCell
            let person = personsToDisplay(indexPath: indexPath)
            cell.avatarImageView.image = UIImage(data: person.photo!)
            cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.bounds.size.width / 2
            cell.avatarImageView.clipsToBounds = true
            cell.namePerson.text = String(person.first_name + " " + person.last_name)
            
            return cell
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showFriendsProfileSegue" {
            
            if let indexPath = tableView.indexPathForSelectedRow {
                let dvc = segue.destination as! ProfileViewController
                dvc.person = personsToDisplay(indexPath: indexPath)
            }
        } 
    }
    
}
