//
//  SearchPersonsTableViewController.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 22.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import UIKit

class SearchPersonsTableViewController: UITableViewController {
    
    private var searchController: UISearchController!
    private var searchVK                      = SearchVK()
    private var persons: [User]               = []

    private var mainUser: User?
    private var mainUserIsLoad        = false

    private var currentPage           = 0
    private var shouldShowLoadingCell = false
    private var searchQ               = ""
    
    
    override func viewDidAppear(_ animated: Bool) {
        searchController.isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl?.backgroundColor = #colorLiteral(red: 0.3163684011, green: 0.6845170259, blue: 0.8394302726, alpha: 1)
        refreshControl?.tintColor = .white
        refreshControl?.addTarget(self, action: #selector(reloadSceneData), for: .valueChanged)
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        searchController.searchBar.delegate = self
        searchController.searchBar.barTintColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        searchController.searchBar.tintColor = .white
        searchController.delegate = self
        
        refreshControl?.beginRefreshing()
        loadMainUser()
        loadResults()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath) else { return }
        fetchNextPage()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = persons.count
        return shouldShowLoadingCell ? count + 1 : count
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if isLoadingIndexPath(indexPath) {
            
            return LoadingCell(style: .default, reuseIdentifier: "loading")
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PersonTableViewCell
            let person = personsToDisplay(indexPath: indexPath)
            cell.avatarImageView.image = UIImage(data: person.photo!)
            cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.bounds.size.width / 2
            cell.avatarImageView.clipsToBounds = true
            cell.namePerson.text = String(person.first_name + " " + person.last_name)
            
            return cell
        }
    }
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == self.persons.count
    }
    func personsToDisplay(indexPath: IndexPath) -> User{
        return persons[indexPath.row]
    }
    private func fetchNextPage() {
        currentPage += 1
        loadResults()
    }
    private func loadResults(refresh: Bool = false) {
        searchVK.search(request: searchQ, offset: 20*currentPage, count: 20) { (users) in
            DispatchQueue.main.async {
                if refresh {
                    self.persons = users
                } else {
                    self.persons.append(contentsOf: users)
                }
                self.shouldShowLoadingCell = self.currentPage < self.searchVK.countOfSearch/20 + 1
                
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    private func loadMainUser(){
        if (!mainUserIsLoad) {
            let uvk = UserVK()
            let userDef = UserDefaults.standard
            
            guard let userID = userDef.string(forKey: "VKAccessUserId") else {return}
            uvk.getUserInfo(userID: userID, completion: { (user) in
                DispatchQueue.main.async {
                    self.mainUser = user
                    self.mainUserIsLoad = true
                }
                
            })
        }
    }
    
    @objc func reloadSceneData() {
        currentPage = 0
        loadResults(refresh: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchQ = searchController.searchBar.text!
        currentPage = 0
        loadResults(refresh:true)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "profileSegue" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let dvc = segue.destination as! ProfileViewController
                dvc.person = personsToDisplay(indexPath: indexPath)
            }
        } else if segue.identifier == "myProfileSegue" {
            if mainUserIsLoad {
                let dvc = segue.destination as! ProfileViewController
                dvc.person = mainUser
            }
        }
    }
    
}

extension SearchPersonsTableViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            navigationController?.hidesBarsOnSwipe = false
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        navigationController?.hidesBarsOnSwipe = true
        
    }
}
extension SearchPersonsTableViewController: UISearchControllerDelegate {
    func didPresentSearchController(_ searchController: UISearchController) {
        self.searchController.searchBar.becomeFirstResponder()
    }
}
