//
//  StartViewController.swift
//  IncubateTest
//
//  Created by Александра Башкирова on 22.11.2017.
//  Copyright © 2017 Alexandra Bashkirova. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    var isAuth: Bool!
    @IBAction func authPressed(_ sender: UIButton) {
        let vkAuthWC = VKWebView()
        vkAuthWC.viewController = self
        self.present(vkAuthWC, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = UserDefaults.standard
        self.isAuth = userDefaults.bool(forKey: "VKIsAccess")
        
        if (isAuth) {
            loadApp()
        }
    }
    
    func authComplete() {
        
        isAuth = true
        loadApp()
    }
    func loadApp(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "tabBarController")
        DispatchQueue.main.async{
            self.present(vc!,animated: true, completion:nil)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}
